package hw.spark;

import scala.Tuple2;

import org.apache.commons.lang.time.DateUtils;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.SparkSession;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Program which calculate linux syslog priority 
 * (is given by 7 – debug, 6 – info, 5 – notice, 4 - warning, warn, 3 - err, error, 2 - crit, 1 - alert,  0 - emerg, panic) 
 * count by hours.
 * 
 * @author Fedor Trofimov
 *
 */

public final class LogCounter {

	private static final SimpleDateFormat sdf = new SimpleDateFormat(
			"MMM dd HH:00");
	
	/**
	 * @param args, first element - a directory or a file of input data, second one - output file
	 */

	public static void main(String[] args) throws Exception {

		if (args.length < 2) {
			System.err
					.println("Usage: JavaLogCounter <input syslog> <output file>");
			System.exit(1);
		}

		SparkSession spark = SparkSession.builder().appName("JavaLogCounter")
				.getOrCreate();

		JavaSparkContext jsc = new JavaSparkContext(spark.sparkContext());
		

		JavaPairRDD<LongWritable, Text> rdd = jsc.sequenceFile(args[0],
				LongWritable.class, Text.class);
		
		JavaPairRDD<Long, String> lines = rdd.mapToPair(r -> new Tuple2<>(r._1.get(), r._2.toString()));

		JavaPairRDD<String, Iterable<String>> pureDatePriorCounts = process(lines);

		pureDatePriorCounts.saveAsTextFile(args[1]);

		List<Tuple2<String, Iterable<String>>> datePriorCountsList = pureDatePriorCounts
				.collect();

		if (datePriorCountsList.size() != 0) {
			System.out.println("result: ");
			for (Tuple2<?, ?> item : datePriorCountsList) {
				System.out.println("\t" + item._1() + " " + item._2());
			}
		} else {
			System.out.println("result is empty!");
		}

		jsc.close();

		spark.stop();
	}
	
	/**
	 * Core method that calculates target RDD
	 * 
	 * @param rdd Java RDD with timestamp of log writing's moment and message content with rsyslog
	 * format "%syslogpriority%,%syslogfacility%,%timegenerated%,%HOSTNAME%,%syslogtag%,%msg%\n"
	 * @return target RDD with date of log writing's moment and list of priority statistic as [ into - 300 , warning - 2,  ...]
	 */
	public static JavaPairRDD<String, Iterable<String>> process(JavaPairRDD<Long, String> rdd){
		JavaPairRDD<Tuple2<Integer, Date>, Integer> priorDateOne = rdd
				.mapToPair(l -> new Tuple2<>(parseKey(l._1,
						l._2), 1));

		JavaPairRDD<Tuple2<Integer, Date>, Integer> priorDateCount = priorDateOne
				.reduceByKey((l1, l2) -> l1 + l2);

		JavaPairRDD<Date, String> datePriorCount = priorDateCount
				.mapToPair(c -> new Tuple2<>(c._1._2, LogCounter
						.priorityIntToString(c._1._1) + " - " + c._2));

		JavaPairRDD<Date, Iterable<String>> datePriorCounts = datePriorCount
				.groupByKey().sortByKey();
		
		return datePriorCounts.mapToPair(l -> new Tuple2<>(LogCounter.sdf.format(l._1), l._2));
	}
	
	/**
	 * 
	 * @param time timstamp of message
	 * @param log message with first number of priority
	 * @return pair of log priority and date
	 */
	private static Tuple2<Integer, Date> parseKey(Long time, String log) {
		return new Tuple2<>(Integer.valueOf(log.substring(0, 1)),
				DateUtils.truncate(new Date(time), Calendar.HOUR));
	}

	/**
	 * 
	 * @param num syslog priority in digit view
	 * @return num syslog priority in string view
	 */
	private static String priorityIntToString(int num) {
		String str;
		switch (num) {
		case 0:
			str = "emerg";
			break;
		case 1:
			str = "alert";
			break;
		case 2:
			str = "crit";
			break;
		case 3:
			str = "error";
			break;
		case 4:
			str = "warning";
			break;
		case 5:
			str = "notice";
			break;
		case 6:
			str = "info";
			break;
		case 7:
			str = "debug";
			break;
		default:
			str = "another";
			break;
		}
		return str;
	}
}