package hw.spark;

import hw.spark.LogCounter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.junit.Test;

import scala.Tuple2;
import scala.reflect.ClassTag;

import com.holdenkarau.spark.testing.JavaRDDComparisons;
import com.holdenkarau.spark.testing.SharedJavaSparkContext;

public class LogCounterTest extends SharedJavaSparkContext implements Serializable {

	private static final long serialVersionUID = 1L;

	@Test
	public void testForOneHour() throws ClassNotFoundException {
		
		
		List<Tuple2<Long, String>> inputList = new ArrayList<Tuple2<Long, String>>();
		inputList.add(new Tuple2<Long, String>(
						new Long(1512165158000L),
						new String("4,0,Dec  2 00:52:38,fedor-VirtualBox,kernel:,[15987.224287] failed. rc=-2")));
		inputList.add(new Tuple2<Long, String>(
				new Long(1512165300000L),
				new String("4,0,Dec  2 00:55:00,fedor-VirtualBox,test")));
		JavaPairRDD<Long, String> inputRDD = jsc().parallelizePairs(inputList);

		JavaPairRDD<String, Iterable<String>> result = LogCounter
				.process(inputRDD);
		
		JavaPairRDD<String, String> resultRDD = result.mapToPair(r -> new Tuple2<>(r._1, r._2.iterator().next()));
		
		List<Tuple2<String, String>> expectedList = Arrays
				.asList(new Tuple2<String, String>("Dec 02 00:00","warning - 2"));

		JavaPairRDD<String, String> expectedRDD = jsc().parallelizePairs(expectedList);
		
		ClassTag<Tuple2<String, String>> tag = scala.reflect.ClassTag$.MODULE$
				.apply(Tuple2.class); 
		
		JavaRDDComparisons.assertRDDEquals(JavaRDD.fromRDD(JavaPairRDD.toRDD(resultRDD), tag), 
				JavaRDD.fromRDD(JavaPairRDD.toRDD(expectedRDD), tag));

	}
	
	@Test
	public void testForSeveralHours() throws ClassNotFoundException {
		
		
		List<Tuple2<Long, String>> inputList = new ArrayList<Tuple2<Long, String>>();
		inputList.add(new Tuple2<Long, String>(
						new Long(1512165158000L),
						new String("4,0,Dec  2 00:52:38,fedor-VirtualBox,kernel:,[15987.224287] failed. rc=-2")));
		inputList.add(new Tuple2<Long, String>(
				new Long(1512165764000L),
				new String("4,0,Dec  2 01:02:01,fedor-VirtualBox,test")));
		JavaPairRDD<Long, String> inputRDD = jsc().parallelizePairs(inputList);

		JavaPairRDD<String, Iterable<String>> result = LogCounter
				.process(inputRDD);
		
		JavaPairRDD<String, String> resultRDD = result.mapToPair(r -> new Tuple2<>(r._1, r._2.iterator().next()));
		
		List<Tuple2<String, String>> expectedList = Arrays
				.asList(new Tuple2<String, String>("Dec 02 00:00","warning - 1"),
						new Tuple2<String, String>("Dec 02 01:00","warning - 1"));

		JavaPairRDD<String, String> expectedRDD = jsc().parallelizePairs(expectedList);
		
		ClassTag<Tuple2<String, String>> tag = scala.reflect.ClassTag$.MODULE$
				.apply(Tuple2.class); 
		
		JavaRDDComparisons.assertRDDEquals(JavaRDD.fromRDD(JavaPairRDD.toRDD(resultRDD), tag), 
				JavaRDD.fromRDD(JavaPairRDD.toRDD(expectedRDD), tag));

	}

}
